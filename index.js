const puppeteer = require('puppeteer');
const fs = require('fs');

(async () => {
  const dir = './screens';
  const URL = 'https://slavals.herokuapp.com/posts'

  if(!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

  const browser = await puppeteer.launch({
    headless: false,
    ignoreHTTPSErrors: true,
  });

  const page = await browser.newPage();

  page.setViewport({
    width: 1360,
    height: 960
  });

  await page.goto(URL, {
    waitUntil: ['networkidle0', 'domcontentloaded'],
    //timeout: 30000
  });

  await page.screenshot({
    path: 'screens/example.png'
  });

  await page.screenshot({
    path: 'screens/fullPage.png',
    fullPage: true
  });

  await browser.close();
})();
